package br.com.itau.garagemapp.models;

import java.time.LocalDateTime;
import java.util.UUID;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer;
import com.fasterxml.jackson.databind.ser.std.UUIDSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class Token {
	@JsonSerialize(using=UUIDSerializer.class)
    @JsonDeserialize(using=UUIDDeserializer.class)
	private UUID token;
	
	@JsonSerialize(using=LocalDateTimeSerializer.class)
	@JsonDeserialize(using=LocalDateTimeDeserializer.class)
	private LocalDateTime dataHoraInicial;
	
	@JsonSerialize(using=LocalDateTimeSerializer.class)
	@JsonDeserialize(using=LocalDateTimeDeserializer.class)
	private LocalDateTime dataHoraFinal;
	
	private boolean status;
	
	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	protected Token() { }
	
	public Token(LocalDateTime dataHoraInicial, LocalDateTime dataHoraFinal) {
		setToken(UUID.randomUUID());
		setDataHoraInicial(dataHoraInicial);
		setDataHoraFinal(dataHoraFinal);
		setStatus(true);
	}
	
	
	public UUID getToken() {
		return token;
	}
	protected void setToken(UUID token) {
		this.token = token;
	}	
	public LocalDateTime getDataHoraInicial() {
		return dataHoraInicial;
	}
	public void setDataHoraInicial(LocalDateTime dataHoraInicial) {
		this.dataHoraInicial = dataHoraInicial;
	}
	public LocalDateTime getDataHoraFinal() {
		return dataHoraFinal;
	}
	public void setDataHoraFinal(LocalDateTime dataHoraFinal) {
		this.dataHoraFinal = dataHoraFinal;
	}
	
	
}
