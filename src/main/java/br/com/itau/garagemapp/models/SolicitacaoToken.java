package br.com.itau.garagemapp.models;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.format.DateTimeFormatter;

public class SolicitacaoToken {
	private Polo polo;
	private LocalDateTime dataHoraInicio;
	private LocalDateTime dataHoraFim;
	private Token token;
		
	public SolicitacaoToken(Polo polo, LocalDateTime dataHoraInicio, LocalDateTime dataHoraFim) {
		setPolo(polo);
		setDataHoraInicio(dataHoraInicio);
		setDataHoraFim(dataHoraFim);
	}
		
	public Polo getPolo() {
		return polo;
	}
	public void setPolo(Polo polo) {
		this.polo = polo;
	}	
	public LocalDateTime getDataHoraInicio() {
		return dataHoraInicio;
	}
	public void setDataHoraInicio(LocalDateTime dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}
	public LocalDateTime getDataHoraFim() {
		return dataHoraFim;
	}
	public void setDataHoraFim(LocalDateTime dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}
	public Token getToken() {
		return token;
	}
	public void setToken(Token token) {
		this.token = token;
	}	
}
