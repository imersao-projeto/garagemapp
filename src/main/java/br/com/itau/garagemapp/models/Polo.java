package br.com.itau.garagemapp.models;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Polo {
	
	@Id @GeneratedValue
	private Long id;	
	private String descricao;	
	@Relationship(type="AUTORIZADO_POR", direction=Relationship.OUTGOING)
	private List<Politica> politicas; 

	
	protected Polo() { }
	
	public Polo(String descricao) {
		this(descricao, new ArrayList<Politica>());
	}

	public Polo(String descricao, List<Politica> politicas) {
		setDescricao(descricao);
		setPoliticas(politicas);
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Politica> getPoliticas() {
		return politicas;
	}

	protected void setPoliticas(List<Politica> politicas) {
		this.politicas = politicas;
	}
}
