package br.com.itau.garagemapp.models;

import java.io.IOException;
import java.time.LocalDate;


import java.util.HashMap;
import java.util.Map;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@NodeEntity
public class Politica {
	
	@Id @GeneratedValue
	private Long id;
	private String descricao;
	private String atributosPolitica;	
//	private String cargo;
//	private String userId;
	private LocalDate dataInicioVigencia;
	private LocalDate dataFinalVigencia;
	
	protected Politica() { }
		
	public Politica(String descricao, Map<String, String> atributosPolitica, LocalDate dataInicioVigencia,
			LocalDate dataFinalVigencia) {
		setDescricao(descricao);
		setAtributosPolitica(atributosPolitica);
		setDataInicioVigencia(dataInicioVigencia);
		setDataFinalVigencia(dataFinalVigencia);
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	protected String getAtributosPoliticaSerializado() {
		return atributosPolitica;
	}

	protected void setAtributosPoliticaSerializado(String atributosPoliticaSerializado) {
		this.atributosPolitica = atributosPoliticaSerializado;
	}
	
	public Map<String, String> getAtributosPolitica() {
		TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<String, String> map = mapper.readValue(getAtributosPoliticaSerializado(), typeRef);
			return map;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void setAtributosPolitica(Map<String, String> atributos) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(atributos);
			setAtributosPoliticaSerializado(json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setAtributosPoliticaSerializado("");
		}		
	}
	public LocalDate getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	public void setDataInicioVigencia(LocalDate dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	public LocalDate getDataFinalVigencia() {
		return dataFinalVigencia;
	}
	public void setDataFinalVigencia(LocalDate validade) {
		this.dataFinalVigencia = validade;
	}	
}
