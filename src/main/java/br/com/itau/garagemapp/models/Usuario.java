package br.com.itau.garagemapp.models;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@NodeEntity
public class Usuario {
	@Id @GeneratedValue
	private Long id;	
	private String userId;
	private String senha;
	private String nome;
	private String atributos;
	private String token;
	@Relationship(type="ENQUADRADO_EM", direction=Relationship.OUTGOING)
	private List<Politica> politicas;
	
	//private String email;			
	//private String lotacao;	
	//private String cargo;	
	
	public Usuario() { }
			
	public Usuario(String userId, String senha) {
		this(userId, senha, "", new HashMap<String, String>(), new ArrayList<Politica>());
	}
	
	public Usuario(String userId, String senha, String nome, Map<String, String> atributos) {
		this(userId, senha, nome, atributos, new ArrayList<Politica>());
	}
	
	public Usuario(String userId, String senha, String nome, Map<String, String> atributos, List<Politica> politicas) {
		setUserId(userId);
		setSenha(senha);
		setNome(nome);
		setAtributos(atributos);
		setPoliticas(politicas);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}	

	protected String getAtributosSerializado() {
		return atributos;
	}

	protected void setAtributosSerializado(String atributosSerializado) {
		this.atributos = atributosSerializado;
	}
	
	public Map<String, String> getAtributos() {
		TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<String, String> map = mapper.readValue(getAtributosSerializado(), typeRef);
			return map;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void setAtributos(Map<String, String> atributos) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(atributos);
			setAtributosSerializado(json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setAtributosSerializado("");
		}		
	}

	protected String getTokenSerializado() {
		if (token == null) {
			token = "";
		}
		return token;
	}

	protected void setTokenSerializado(String tokenSerializado) {
		this.token = tokenSerializado;
	}
	
	public Token getToken() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Token map = mapper.readValue(getTokenSerializado(), Token.class);
			return map;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void queimarToken() {
		Token t = getToken();
		t.setStatus(false);
		
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(t);
			setTokenSerializado(json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setTokenSerializado("");
		}
	}
	
	public void gerarTokenAcesso(LocalDateTime dataHoraInicio, LocalDateTime dataHoraFim) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			Token token = new Token(dataHoraInicio, dataHoraFim);
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(token);
			setTokenSerializado(json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setTokenSerializado("");
		}		
	}
	
	public void vincularPolitica(Politica politica) {
		this.politicas.add(politica);
	}
	
	protected void setPoliticas(List<Politica> politicas) {
		this.politicas = politicas;
	}
	
	public List<Politica> getPoliticas() {
		return politicas;
	}
}
