package br.com.itau.garagemapp.models;

public class WebToken {
	private String codigo;
	
	public WebToken() {
	}

	public WebToken(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
