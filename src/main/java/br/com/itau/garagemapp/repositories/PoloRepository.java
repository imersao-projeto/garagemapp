package br.com.itau.garagemapp.repositories;

import java.util.Optional;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import br.com.itau.garagemapp.models.Polo;


public interface PoloRepository extends Neo4jRepository<Polo, Long>{
	Optional<Polo> findByDescricao(String descricao);
}
