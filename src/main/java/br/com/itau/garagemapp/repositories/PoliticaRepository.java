package br.com.itau.garagemapp.repositories;

import java.util.Optional;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import br.com.itau.garagemapp.models.Politica;

public interface PoliticaRepository extends Neo4jRepository<Politica, Long> {
	Optional<Politica> findByDescricao(String descricao);
}
