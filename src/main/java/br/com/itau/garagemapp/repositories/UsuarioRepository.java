package br.com.itau.garagemapp.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import br.com.itau.garagemapp.models.Polo;
import br.com.itau.garagemapp.models.Usuario;

public interface UsuarioRepository extends Neo4jRepository<Usuario, Long> {
	Optional<Usuario> findByUserId(String userId);
	
    @Query("MATCH (u:Usuario {userId: {userId}})-[]->(p:Politica)<-[]-(o:Polo) RETURN o")
    Iterable<Polo> getPolosAutorizados(@Param("userId") String userId);
}
