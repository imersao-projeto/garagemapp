package br.com.itau.garagemapp.config;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.garagemapp.models.Mensagem;
import br.com.itau.garagemapp.models.Usuario;
import br.com.itau.garagemapp.repositories.UsuarioRepository;
import br.com.itau.garagemapp.services.JWTService;

@Component
@CrossOrigin
public class CheckWebTokenFilter implements Filter {

	@Autowired
	JWTService jwtService;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;		
		String token = httpRequest.getHeader("Authorization");
		Mensagem mensagem = new Mensagem("Não autorizado.");
		
		// Carregar os serviços caso o Spring ainda não tenha feito isso
		ServletContext servletContext = request.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        
        if(httpRequest.getMethod().equals("OPTIONS")) {
        	chain.doFilter(httpRequest, httpResponse);
        	return;
        }
        
		if (jwtService == null) {			
            jwtService = webApplicationContext.getBean(JWTService.class);
		}
		if (usuarioRepository == null) {
            usuarioRepository = webApplicationContext.getBean(UsuarioRepository.class);
		}
		
		if (token == null) {			
			httpResponse = buildResponse(httpResponse, mensagem, HttpServletResponse.SC_FORBIDDEN);
			return;
		}

		token = token.replace("Bearer ", "");

		String username = jwtService.validarToken(token);

		if (username == null) {
			httpResponse = buildResponse(httpResponse, mensagem, HttpServletResponse.SC_FORBIDDEN);
			return;
		}

		Optional<Usuario> usuarioOptional = usuarioRepository.findByUserId(username);

		if (!usuarioOptional.isPresent()) {			
			httpResponse = buildResponse(httpResponse, mensagem, HttpServletResponse.SC_FORBIDDEN);
			return;
		}		
		
		httpResponse.addHeader("Access-Control-Allow-Origin", "*");
		
		
		chain.doFilter(httpRequest, httpResponse);
	}
	
	private HttpServletResponse buildResponse(HttpServletResponse response, Mensagem msg, int sc) {
		ObjectMapper mapper = new ObjectMapper();		
		String json;
		try {			
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(msg);
			response.setContentType("application/json");
			response.getWriter().write(json);
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return response;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	

}
