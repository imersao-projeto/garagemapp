package br.com.itau.garagemapp.config;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.itau.garagemapp.models.Politica;
import br.com.itau.garagemapp.models.Polo;
import br.com.itau.garagemapp.models.Usuario;
import br.com.itau.garagemapp.repositories.PoliticaRepository;
import br.com.itau.garagemapp.repositories.PoloRepository;
import br.com.itau.garagemapp.repositories.UsuarioRepository;

@Configuration
public class DatabaseConfig {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PoloRepository poloRepository;
	
	@Autowired
	PoliticaRepository politicaRepository;
	
	
	@Bean
	CommandLineRunner popularDadosIniciais() {
		return args -> {
			
			// Remover estas linhas antes de publicar
			usuarioRepository.deleteAll();
			poloRepository.deleteAll();
			politicaRepository.deleteAll();
			// Remover estas linhas antes de publicar
			
			////////////////////////////
			// POLITICA
			////////////////////////////
			ArrayList<Politica> politicasCTO = new ArrayList<>();
			ArrayList<Politica> politicasCEIC = new ArrayList<>();
			
			HashMap<String, String> atributosPoliticaCTO = new HashMap<>();
			atributosPoliticaCTO.put("cargo", "coordenador");
			Politica politicaCTO = new Politica("PoliticaCTO", atributosPoliticaCTO, 
					LocalDate.of(2018, 01, 01), LocalDate.of(2019, 01, 01));
			
			HashMap<String, String> atributosPoliticaCEIC = new HashMap<>();
			atributosPoliticaCEIC.put("cargo", "coordenador");
			atributosPoliticaCEIC.put("userId", "setubal");
			Politica politicaCEIC = new Politica("PoliticaCTO", atributosPoliticaCEIC, 
					LocalDate.of(2018, 01, 01), LocalDate.of(2019, 01, 01));
			
			politicasCTO.add(politicaCTO);
			politicasCEIC.add(politicaCEIC);
			
			////////////////////////////
			// POLO
			////////////////////////////
			Polo cto = new Polo("Centro Tecnologico", politicasCTO);			
			Polo ceic = new Polo("CEIC", politicasCEIC);
			Polo cab = new Polo("CA Brigadeiro");
			Polo cat = new Polo("CA Tatuape");
			Polo cap = new Polo("CA Pinheiros");
						
			////////////////////////////
			// USUARIOS
			////////////////////////////
			String senha = encoder().encode("1234");
			
			// João
			HashMap<String, String> atributosJoao = new HashMap<>();
			atributosJoao.put("email", "joao@itau.com.br");
			atributosJoao.put("lotacao", "CTO");
			atributosJoao.put("cargo", "coordenador");
			Usuario joao = new Usuario("joao", senha, "João", atributosJoao);
			
			// José
			HashMap<String, String> atributosJose = new HashMap<>();
			atributosJose.put("email", "jose@itau.com.br");
			atributosJose.put("lotacao", "CTO");
			atributosJose.put("cargo", "funcionario");
			Usuario jose = new Usuario("jose", senha, "José", atributosJose);
			
			// Maria
			HashMap<String, String> atributosMaria = new HashMap<>();
			atributosMaria.put("email", "maria@itau.com.br");
			atributosMaria.put("lotacao", "CEIC");
			atributosMaria.put("cargo", "coordenador");
			Usuario maria = new Usuario("maria", senha, "Maria", atributosMaria);
			
			// Joaquim
			HashMap<String, String> atributosJoaquim = new HashMap<>();
			atributosJoaquim.put("email", "joaquim@itau.com.br");
			atributosJoaquim.put("lotacao", "CEIC");
			atributosJoaquim.put("cargo", "funcionario");
			Usuario joaquim = new Usuario("joaquim", senha, "Joaquim", atributosJoaquim);
			
			// Setubal
			HashMap<String, String> atributosSetubal = new HashMap<>();
			atributosSetubal.put("email", "setubal@itau.com.br");
			atributosSetubal.put("lotacao", "CEIC");
			atributosSetubal.put("cargo", "dono");
			Usuario setubal = new Usuario("setubal", senha, "Setubal", atributosSetubal);
			
			
			// Token
			joao.gerarTokenAcesso(LocalDateTime.of(2018, Month.JULY, 01, 18, 00), 
					LocalDateTime.of(2018, Month.JULY, 31, 23, 00));
			jose.gerarTokenAcesso(LocalDateTime.of(2018, Month.JULY, 01, 18, 00), 
					LocalDateTime.of(2018, Month.JULY, 31, 23, 00));
			
			// Vinculos
			
			joao.vincularPolitica(politicaCTO);
			jose.vincularPolitica(politicaCTO);
			maria.vincularPolitica(politicaCEIC);
			setubal.vincularPolitica(politicaCEIC);
			setubal.vincularPolitica(politicaCTO);						
				
			//////////////////////////
			////// POPULAR
			//////////////////////////
			
			if (!politicaRepository.findByDescricao("PoliticaCEIC").isPresent()) {
				politicaRepository.save(politicaCEIC);
			}
			if (!politicaRepository.findByDescricao("PoliticaCTO").isPresent()) {
				politicaRepository.save(politicaCTO);
			}
			
			/////////
			

			if (!poloRepository.findByDescricao("Centro Tecnologico").isPresent()) {
				poloRepository.save(cto);
			}
			if (!poloRepository.findByDescricao("CEIC").isPresent()) {
				poloRepository.save(ceic);
			}
			if (!poloRepository.findByDescricao("CA Brigadeiro").isPresent()) {
				poloRepository.save(cab);
			}
			if (!poloRepository.findByDescricao("CA Tatuape").isPresent()) {
				poloRepository.save(cat);
			}
			if (!poloRepository.findByDescricao("CA Pinheiros").isPresent()) {
				poloRepository.save(cap);
			}
			
			////////
			
			if (!usuarioRepository.findByUserId("joao").isPresent()) {
				usuarioRepository.save(joao);
			}
			if (!usuarioRepository.findByUserId("jose").isPresent()) {
				usuarioRepository.save(jose);
			}
			if (!usuarioRepository.findByUserId("maria").isPresent()) {
				usuarioRepository.save(maria);
			}
			if (!usuarioRepository.findByUserId("joaquim").isPresent()) {
				usuarioRepository.save(joaquim);
			}
			if (!usuarioRepository.findByUserId("setubal").isPresent()) {
				usuarioRepository.save(setubal);
			}
		};
	}
	
	@Bean
    public BCryptPasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
	
}
