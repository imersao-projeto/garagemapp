package br.com.itau.garagemapp.config;


import br.com.itau.garagemapp.models.Usuario;
import br.com.itau.garagemapp.models.UsuarioAtivo;
import br.com.itau.garagemapp.models.WebToken;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig implements WebMvcConfigurer {	
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(usuarioAtivoInterceptor());
    }

    @Bean(name="usuarioAtivo")
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public UsuarioAtivo usuarioAtivo() {
        return new UsuarioAtivo();
    }

    @Bean
    public UsuarioAtivoInterceptor usuarioAtivoInterceptor() {
        return new UsuarioAtivoInterceptor(usuarioAtivo());
    }
}
