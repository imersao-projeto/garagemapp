package br.com.itau.garagemapp.config;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailSenderValidatorAutoConfiguration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.itau.garagemapp.models.Usuario;
import br.com.itau.garagemapp.models.UsuarioAtivo;
import br.com.itau.garagemapp.models.WebToken;
import br.com.itau.garagemapp.repositories.UsuarioRepository;
import br.com.itau.garagemapp.services.JWTService;

public class UsuarioAtivoInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	JWTService jwtService;
	
	private UsuarioAtivo usuarioAtivo;
	
	public UsuarioAtivoInterceptor(UsuarioAtivo usuarioAtivo) {
		this.usuarioAtivo = usuarioAtivo;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String authorizationToken = request.getHeader("Authorization");
		if (authorizationToken != null) {
			if (!authorizationToken.equals("")) {
				authorizationToken = authorizationToken.replace("Bearer ", "");
				String username = jwtService.validarToken(authorizationToken);				
				if (username != null) {
					Optional<Usuario> usuarioOptional = usuarioRepository.findByUserId(username);
					if (usuarioOptional.isPresent()) {
						usuarioAtivo.setUsuario(usuarioOptional.get());
					}
				}
			}
		}        
        return true;
	}
}
