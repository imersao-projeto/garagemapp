package br.com.itau.garagemapp.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
	
	@Bean
	public FilterRegistrationBean<CheckWebTokenFilter> checkTokenFilter(){
	    FilterRegistrationBean<CheckWebTokenFilter> registrationBean = new FilterRegistrationBean<>();
	         
	    registrationBean.setFilter(new CheckWebTokenFilter());
	    registrationBean.addUrlPatterns("/usuarios/*");
	    registrationBean.addUrlPatterns("/polos/*");
	         
	    return registrationBean;    
	}
}
