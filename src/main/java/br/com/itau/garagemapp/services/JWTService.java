package br.com.itau.garagemapp.services;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

@Service
public class JWTService {
	String key = "garagemApp";
	Algorithm algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier = JWT.require(algorithm).build();
	
	public String gerarToken(String userid) {
		return JWT.create().withClaim("userid", userid).sign(algorithm);
	}
	
	public String validarToken(String token) {
		try {
			return verifier.verify(token).getClaim("userid").asString();
		}catch (Exception e) {
			return null;
		}
	}
}
