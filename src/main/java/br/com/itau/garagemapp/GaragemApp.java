package br.com.itau.garagemapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GaragemApp {

	public static void main(String[] args) {
		SpringApplication.run(GaragemApp.class, args);
	}
}
