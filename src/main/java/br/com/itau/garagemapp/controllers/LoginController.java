package br.com.itau.garagemapp.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.garagemapp.models.Mensagem;
import br.com.itau.garagemapp.models.Usuario;
import br.com.itau.garagemapp.repositories.UsuarioRepository;
import br.com.itau.garagemapp.services.JWTService;
import br.com.itau.garagemapp.services.PasswordService;

@RestController
@RequestMapping("/login")
@CrossOrigin
public class LoginController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	JWTService jwtService;

	@PostMapping()
	public ResponseEntity<?> fazerLogin(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBancoOptional = usuarioRepository.findByUserId(usuario.getUserId());

		if (!usuarioBancoOptional.isPresent()) {
			Mensagem mensagem = new Mensagem("Usuario invalido, tente novamente");
			return ResponseEntity.badRequest().body(mensagem);
		}

		Usuario usuarioBanco = usuarioBancoOptional.get();

		if (passwordService.verificarHash(usuario.getSenha(), usuarioBanco.getSenha())) {
			String token = jwtService.gerarToken(usuarioBanco.getUserId());

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");

			return new ResponseEntity<Usuario>(new Usuario(), headers, HttpStatus.OK);
		}
		Mensagem mensagem = new Mensagem("Usuario e/ou senha invalido(s) , tente novamente");
		return ResponseEntity.status(403).body(mensagem);
	}
	
}
