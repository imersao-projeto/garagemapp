package br.com.itau.garagemapp.controllers;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.garagemapp.models.Mensagem;
import br.com.itau.garagemapp.models.Polo;
import br.com.itau.garagemapp.models.SolicitacaoToken;
import br.com.itau.garagemapp.models.Usuario;
import br.com.itau.garagemapp.models.UsuarioAtivo;
import br.com.itau.garagemapp.repositories.PoloRepository;
import br.com.itau.garagemapp.repositories.UsuarioRepository;

@RestController
@RequestMapping("usuarios")
@CrossOrigin
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	PoloRepository poloRepository;
	
	@Autowired
    private UsuarioAtivo usuarioAtivo;
	

	@PostMapping("/solicitar-token-estacionamento")
	public ResponseEntity<?> solicitarTokenGaragem(@RequestBody SolicitacaoToken solicitacaoToken) {
		// Pega o usuário que o interceptor gerou através do JWT
		Usuario usuario = usuarioAtivo.getUsuario();
				
	
		ArrayList<Polo> polos = (ArrayList<Polo>) usuarioRepository.getPolosAutorizados(usuario.getUserId());
		Optional<Polo> poloOptional = poloRepository.findByDescricao(solicitacaoToken.getPolo().getDescricao());

		if (!poloOptional.isPresent()) {
			Mensagem mensagem = new Mensagem("Usuario sem permissao na politica do polo selecionado, tente novamente");
			return ResponseEntity.badRequest().body(mensagem);
		}
		Polo polodb = poloOptional.get();

		if (!polos.contains(polodb)) {
			Mensagem mensagem = new Mensagem("Usuário não possui permissão ao polo selecionado");
			return ResponseEntity.status(401).body(mensagem);
		}

		Map<String, String> usrAtributos = usuario.getAtributos();
		String cargo = usrAtributos.get("cargo");
		if (cargo == null) {
			Mensagem mensagem = new Mensagem("Cargo não encontrado");
			return ResponseEntity.badRequest().body(mensagem);
		}
		
		LocalDateTime dataHoraInicial = solicitacaoToken.getDataHoraInicio();
		LocalDateTime dataHoraFinal = solicitacaoToken.getDataHoraFim();
		
		
		LocalDateTime dataHoraInicialPermitida = dataHoraInicial.withHour(18);
		LocalDateTime dataHoraFinalPermitida = dataHoraInicial.plusDays(1).withHour(6);
		
		
		if (cargo.toLowerCase().equals("funcionario") && 
				(dataHoraInicial.isBefore(dataHoraInicialPermitida) || dataHoraFinal.isAfter(dataHoraFinalPermitida))) {
			Mensagem mensagem = new Mensagem("Horário não permitido para o cargo solicitado");
			return ResponseEntity.badRequest().body(mensagem);
		}
		
		usuario.gerarTokenAcesso(solicitacaoToken.getDataHoraInicio(), solicitacaoToken.getDataHoraFim());
		usuarioRepository.save(usuario);
		solicitacaoToken.setToken(usuario.getToken());
		return ResponseEntity.ok(solicitacaoToken.getToken());
	}	

	@PostMapping("/validar-token-estacionamento")
	public ResponseEntity<?> validarToken(@RequestBody HashMap<String, String> token) {
		// Pega o usuário que o interceptor gerou através do JWT
		Usuario usuario = usuarioAtivo.getUsuario();
		
		String codigoToken = token.get("token");
		Mensagem m = new Mensagem("Favor informar o token que deseja validar");
		if (codigoToken == null) {			
			return ResponseEntity.badRequest().body(m);						
		}		
		if (codigoToken.equals("")) {
			return ResponseEntity.badRequest().body(m);
		}

		LocalDateTime dataFinal = usuario.getToken().getDataHoraFinal();
		LocalDateTime dataInicial = usuario.getToken().getDataHoraInicial();
		LocalDateTime dataAtual = LocalDateTime.now();

		if (usuario.getToken().getStatus() == true) {
			if (codigoToken.equals(usuario.getToken().getToken().toString())
					&& (dataAtual.isAfter(dataInicial) && dataAtual.isBefore(dataFinal))) {
				// Aplicar regra para queimar token do funcionario quando utilizado
				String cargo = usuario.getAtributos().get("cargo");
				if (cargo == null) {
					Mensagem mensagem = new Mensagem("Cargo não informado");
					return ResponseEntity.badRequest().body(mensagem);
				} else {
					if (!cargo.toLowerCase().equals("coordenador")) {
						usuario.queimarToken();
						usuarioRepository.save(usuario);
					}
				}
				return ResponseEntity.ok().build();

			} else {
				if (dataInicial.isAfter(dataAtual)) {
					Mensagem mensagem = new Mensagem("Seu período de utilização do token ainda não iniciou");
					return ResponseEntity.status(423).body(mensagem);
				} else {			
					usuario.queimarToken();
					usuarioRepository.save(usuario);
					Mensagem mensagem = new Mensagem("O voucher para acesso informado venceu. Abra outra solicitacao ");
					return ResponseEntity.status(423).body(mensagem);
				}
			}

		}
		Mensagem mensagem = new Mensagem("O voucher para acesso informado ja foi utilizado. Abra outra solicitacao");
		return ResponseEntity.status(403).body(mensagem);
	}

	// Metodo para listar todos os tokens do cliente
	@GetMapping("/tokens")
	public ResponseEntity<?> getTokens() {
		// Pega o usuário que o interceptor gerou através do JWT
		Usuario usuario = usuarioAtivo.getUsuario();
		
		Optional<Usuario> usuarioLogin = usuarioRepository.findByUserId(usuario.getUserId());
		if (!usuarioLogin.isPresent()) {
			Mensagem mensagem = new Mensagem("Usuario não encontrado.");
			return ResponseEntity.badRequest().body(mensagem);
		}

		Usuario usuarioLogado = usuarioLogin.get();
		
		if (usuarioLogado.getToken() == null) {
			Mensagem msg = new Mensagem("Usuário não possui nenhum token de acesso. Favor solicitar");
			return ResponseEntity.status(404).body(msg);
		}

		return ResponseEntity.ok(usuarioLogado.getToken());
				
		
	}
	
	

}
