package br.com.itau.garagemapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.garagemapp.models.Polo;
import br.com.itau.garagemapp.models.UsuarioAtivo;
import br.com.itau.garagemapp.repositories.PoloRepository;

@RestController
@RequestMapping("polos")
@CrossOrigin
public class PoloController {

	@Autowired
	UsuarioAtivo usuarioAtivo;
	
	@Autowired
	PoloRepository poloRepository;
		
	// Método para listar todos os polos disponíveis
	@GetMapping("")
	@CrossOrigin
	public ResponseEntity<Iterable<Polo>> getPolos() {
		return ResponseEntity.ok(poloRepository.findAll());
	}
}
